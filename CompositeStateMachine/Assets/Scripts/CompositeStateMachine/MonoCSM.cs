﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonoCSM : MonoBehaviour
{
    public RootCSM root = new RootCSM();
    private void Awake()
    {
        root.Initialize(new StartingState());
    }
}

public abstract class ExampleCSM<T> : CSM<T> where T : CompositeStateMachine
{
    protected override void Enter()
    {
        base.Enter();
        Debug.Log("Enter to " + this);
    }

    protected override void Exit()
    {
        base.Exit();
        Debug.Log("Exit from " + this);
    }

    protected override void AddListeners()
    {
        base.AddListeners();
        this.AddObserver(KeyDown_Response, GeneralInput.KeyDown_Notification);
    }

    protected override void RemoveListeners()
    {
        base.RemoveListeners();
        this.RemoveObserver(KeyDown_Response, GeneralInput.KeyDown_Notification);
    }
                                                     
    private  void KeyDown_Response(object inputNotificationSender, object keyCordArg)
    {
     
    }

    protected abstract void OpcionA();

    protected abstract void OpcionB();

    protected abstract void OpcionC();

}


public class StartingState : ExampleCSM<CompositeStateMachine>
{
    protected override void Enter()
    {
        base.Enter();
        //SwitchState();
    }

    protected override void OpcionA()
    {
        Debug.Log("A");
    }

    protected override void OpcionB()
    {
        Debug.Log("B");
    }

    protected override void OpcionC()
    {
        Debug.Log("C");
    }
}

