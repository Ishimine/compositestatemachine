﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public abstract class CompositeStateMachine
    {
        public CompositeStateMachine SuperState { get; set; }
        public CompositeStateMachine SubState { get; set; }

        public Action OnEnter { get; set; }
        public Action OnExit { get; set; }

        protected CompositeStateMachine SwitchState(CompositeStateMachine nState)
        {
            SuperState.SwitchSubState(nState);
            return nState;
        }

        protected CompositeStateMachine SwitchSubState(CompositeStateMachine nState)
        {
            SubState?._Exit();
            SetSubState(nState);
            SubState?._Enter();
            return SubState;
        }

        private void SetSubState(CompositeStateMachine nState)
        {
            SubState = nState;
            nState.SuperState = this;
        }

        private void _Enter()
        {
            AddListeners();
            Enter();
            OnEnter?.Invoke();
        }

        private  void _Exit()
        {
            SubState?._Exit();
            RemoveListeners();
            Exit();
            OnEnter?.Invoke();
        }

        protected abstract void Enter();
        protected abstract void Exit();

        protected virtual void OnDestroy()
        {
            RemoveListeners();
        }

        protected virtual void AddListeners()
        {
        }

        protected virtual void RemoveListeners()
        {
        }
    }

public class RootCSM : CompositeStateMachine
{
    public void Initialize(CompositeStateMachine startingState)
    {
        SwitchSubState(startingState);
    }

    protected override void Enter()
    {
        Debug.Log("<color=yellow> Root </color>");
    }

    protected override void Exit()
    {
    }
}

    public class CSM<T> : CompositeStateMachine where T :CompositeStateMachine
    {
        public T Owner => SuperState as T;

        protected override void Enter()
        {
        }

        protected override void Exit()
        {
        }
    }
